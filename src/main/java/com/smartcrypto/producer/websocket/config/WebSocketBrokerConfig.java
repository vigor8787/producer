package com.smartcrypto.producer.websocket.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import com.smartcrypto.producer.websocket.domain.StompTopic;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketBrokerConfig implements WebSocketMessageBrokerConfigurer {
	
	private static final String WEBSOCKET_END_POINT_NAME = "test";
	private static final String MESSAGE_MAPPIN_PREFIX = "/app";

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker(StompTopic.TOPIC_PREFIX);
        config.setApplicationDestinationPrefixes(MESSAGE_MAPPIN_PREFIX);
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint(WEBSOCKET_END_POINT_NAME).withSockJS();
    }

}