package com.smartcrypto.producer.websocket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smartcrypto.producer.websocket.domain.ContractDetails;
import com.smartcrypto.producer.websocket.domain.StompTopic;
import com.smartcrypto.producer.websocket.domain.WebsocketMsg;

@Component
public class WebSocketMsgService {
	
	@Autowired
	private SimpMessagingTemplate template;
	
	public void sendMsgToSampleTopicSubscriber(String msg) {
		template.convertAndSend(StompTopic.STOMP_TOPIC_SAMPLE, new WebsocketMsg(msg));
	}
	
	public void sendContractDetails(ContractDetails contractDetails) {
		ObjectMapper om = new ObjectMapper();
		try {
			this.sendMsgToSampleTopicSubscriber(om.writeValueAsString(contractDetails));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
}
