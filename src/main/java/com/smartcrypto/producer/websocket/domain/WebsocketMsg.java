package com.smartcrypto.producer.websocket.domain;

public class WebsocketMsg {

	  private String msg;

	    public WebsocketMsg() {
	    }

	    public WebsocketMsg(String msg) {
	        this.msg = msg;
	    }

	    public String getMsg() {
	        return msg;
	    }
	    
	    public void setMsg(String msg) {
	    	this.msg = msg;
	    }
}
