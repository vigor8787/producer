package com.smartcrypto.producer.websocket.domain;

public class StompTopic {
	
	public static final String TOPIC_PREFIX = "/topic";
	
	public static final String STOMP_TOPIC_SAMPLE = TOPIC_PREFIX + "/sample";
}
