package com.smartcrypto.producer.websocket.domain;

// TODO : 나중에 롬복으로 좀 바꿔주세요
public class ContractDetails {
	private String kind;
	private String price;
	private String count;
	
	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
}
