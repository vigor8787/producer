package com.smartcrypto.producer.websocket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.HtmlUtils;

import com.smartcrypto.producer.websocket.domain.ContractDetails;
import com.smartcrypto.producer.websocket.domain.StompTopic;
import com.smartcrypto.producer.websocket.domain.WebsocketMsg;
import com.smartcrypto.producer.websocket.service.WebSocketMsgService;

@RestController
public class WebsocketTestController {

	@Autowired
	private WebSocketMsgService webSocketMsgService;

	@MessageMapping("/msg")
	@SendTo(StompTopic.STOMP_TOPIC_SAMPLE)
	public WebsocketMsg greeting(WebsocketMsg message) throws Exception {
		return new WebsocketMsg("[MSG] " + message.getMsg());
	}

	@RequestMapping("/test")
	public String stompTest() throws Exception {
		ContractDetails contractDetails = new ContractDetails();
		contractDetails.setCount("10");
		contractDetails.setKind("�ż�");
		contractDetails.setPrice("20,100");
		webSocketMsgService.sendContractDetails(contractDetails);
		return "stomp test done.";
	}
}
