package com.smartcrypto.producer.websocket;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Mono;


/**
 * https://github.com/wahengchang/nodejs-websocket-example
 * */

public class ReactiveWebSocketHandler implements WebSocketHandler {
    public ReactiveWebSocketHandler(){}

    private static WebSocketSession session;
    private static Publisher<WebSocketMessage> msgPub;

    @Override
    public Mono<Void> handle(WebSocketSession session) {
        WebSocketMessage message = session.textMessage("sended From Server.");
        this.session = session;

        return doSend(session, Mono.just(message));
    }

    private static Mono<Void> doSend(WebSocketSession session, Publisher<WebSocketMessage> output) {
        return session.send(output);
        // workaround for suspected RxNetty WebSocket client issue
        // https://github.com/ReactiveX/RxNetty/issues/560
        // return session.send(Mono.delay(Duration.ofMillis(100)).thenMany(output));
    }

    public static String sendMessage(String msg) {
        if(session != null) {
            session.textMessage(msg);
        }
        return "sended Msg.";
    }
}
