package com.smartcrypto.producer.ethtx;


import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.abi.datatypes.generated.Uint8;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthAccounts;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.ChainId;
import org.web3j.tx.ClientTransactionManager;
import org.web3j.tx.ReadonlyTransactionManager;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.DefaultGasProvider;
import rx.Observable;
import rx.Subscription;

import java.io.IOException;
import java.math.BigInteger;

import java.util.concurrent.Future;

/**
 * https://docs.web3j.io/infura.html
 * Main Ethereum Network:
 * https://mainnet.infura.io/<your-token>
 * Test Ethereum Network (Rinkeby):
 * https://rinkeby.infura.io/<your-token>
 * Test Ethereum Network (Kovan):
 * https://kovan.infura.io/<your-token>
 * Test Ethereum Network (Ropsten):
 * https://ropsten.infura.io/<your-token>
 *
 * Web3j web3 = Web3j.build(new HttpService("https://rinkeby.infura.io/<your-token>"));
 * Web3ClientVersion web3ClientVersion = web3.web3ClientVersion().send();
 * System.out.println(web3ClientVersion.getWeb3ClientVersion());
 *
 *
 * */



@Component
public class Web3jTx {
    private Web3j web3;
    public Web3jTx() {
        System.out.println("Scanned()");

        // defaults to http://localhost:8545/
        // TODO : Robsten 테스트넷
        web3 = Web3j.build(new HttpService());
    }

    public String getVersion() {
        web3.web3ClientVersion().observable().subscribe(x -> {
            String clientVersion = x.getWeb3ClientVersion();
            System.out.println("clientVersion : " + clientVersion);
        });
        return "version";
    }

    public String deployERC20Token() throws IOException, CipherException {
        //Credentials credentials = WalletUtils.loadCredentials("", "");
        EthAccounts accounts = web3.ethAccounts().send();
        String accountAddr = accounts.getAccounts().get(0);
        ClientTransactionManager transactionManager = new ClientTransactionManager(web3, accountAddr);
        //ReadonlyTransactionManager transactionManager = new ReadonlyTransactionManager(web3, accountAddr);
        BigInteger gasPrice = BigInteger.valueOf(4700000); //  0.0000000000047 ETHER
        BigInteger gasLimit = BigInteger.valueOf(3100000); // 0.0000000000031 ETHER
        Uint256 totalSupply = new Uint256(1000000);
        Utf8String tokenName = new Utf8String("my-token");
        Uint8 decimalUnits = new Uint8(8);
        Utf8String tokenSymbol = new Utf8String("mtk");

        //ERC20Token contract = ERC20Token.load("0x<address>|<ensName>", web3, transactionManager, gasPrice, gasLimit);

        //RemoteCall<ERC20Token> token = ERC20Token.deploy(web3, transactionManager, gasPrice, gasLimit).send();
        //Observable<ERC20Token> myToken = token.observable();
        ERC20Token contract = null;
        try {
            contract = ERC20Token.deploy(web3, transactionManager, DefaultGasProvider.GAS_PRICE, DefaultGasProvider.GAS_LIMIT).send();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            String contractAddress = contract.getContractAddress();
            System.out.println("Deployed Contract Address : " + contractAddress);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "deploy";
    }

    public String loadERC20Token() throws IOException, CipherException {
        EthAccounts accounts = web3.ethAccounts().send();
        String accountAddr = accounts.getAccounts().get(0);
        ClientTransactionManager transactionManager = new ClientTransactionManager(web3, accountAddr);
        BigInteger gasPrice = BigInteger.valueOf(4700000); //  0.0000000000047 ETHER
        BigInteger gasLimit = BigInteger.valueOf(3100000); // 0.0000000000031 ETHER
        ERC20Token contract = ERC20Token.load(
                "0x69cd1414806fd4a33a59ec9436aac173d79b602e", web3, transactionManager, DefaultGasProvider.GAS_PRICE, DefaultGasProvider.GAS_LIMIT);
        System.out.println("totalSupply" + contract.totalSupply());

        return "load";
    }

    public void sendTransaction() {
        //TODO : https://medium.com/yopiter/interfacing-with-ethereum-smart-contracts-in-java-cf39b2e95b4e
        //Using the web3j Command Line Tools(https://docs.web3j.io/command_line.html)
        //It will create a .java file with the following contents
        web3.ethSendTransaction(new Transaction(
                "",new BigInteger("0"),new BigInteger("0"),new BigInteger("0"),"",new BigInteger("0"),""
        )).observable().subscribe(x -> {
            System.out.println("Transaction Commited.");
        });
    }
}
