package com.smartcrypto.producer.controller;

import com.smartcrypto.producer.consumer.SampleConsumer;
import com.smartcrypto.producer.ethtx.Web3jTx;
import com.smartcrypto.producer.producer.SampleProducer;
import com.smartcrypto.producer.websocket.ReactiveWebSocketHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class OrderController {
    @GetMapping("/order/producer")
    public Mono buy() throws Exception {
        return (Mono) Mono.just(SampleProducer.order());
        //return Mono.just("buy called()");
    }
    @GetMapping("/order/consumer")
    public Mono sell() throws Exception {
        return Mono.just(SampleConsumer.order());
        //return Mono.just("sell called()");
    }

    @GetMapping("/wc/send")
    public Mono sendMessage() throws Exception {
        return Mono.just(ReactiveWebSocketHandler.sendMessage("test"));
    }

    @GetMapping("web3j/version")
    public Mono getVersion() throws Exception {
        Web3jTx web3j = new Web3jTx();
        return Mono.just(web3j.getVersion());
    }

    @GetMapping("web3j/deploy")
    public Mono deploy() throws Exception {
        Web3jTx web3j = new Web3jTx();
        return Mono.just(web3j.deployERC20Token());
    }

    @GetMapping("web3j/load")
    public Mono load() throws Exception {
        Web3jTx web3j = new Web3jTx();
        return Mono.just(web3j.loadERC20Token());
    }

}